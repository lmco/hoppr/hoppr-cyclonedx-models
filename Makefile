# SPDX-FileCopyrightText: Copyright © 2023 Lockheed Martin <open.source@lmco.com>
# SPDX-FileName: Makefile
# SPDX-FileType: SOURCE
# SPDX-License-Identifier: MIT

BASH := ${shell type -p bash}
SHELL := ${BASH}
MAKEFILE ?= ${abspath ${firstword ${MAKEFILE_LIST}}}

POETRY ?= poetry
BLACK ?= ${POETRY} run black
MYPY ?= ${POETRY} run mypy
PYLINT ?= ${POETRY} run pylint
PYTEST ?= ${POETRY} run pytest

TARGETS := hoppr_cyclonedx_models tests

# ANSI color escape codes
BOLD ?= \033[1m
CYAN ?= \033[36m
GREEN ?= \033[32m
RED ?= \033[31m
YELLOW ?= \033[33m
NC ?= \033[0m # No Color

.PHONY: all build clean help format test
.SILENT: clean

#@ Tools
help: # Display this help
	@awk 'BEGIN {FS = ":.*#"; printf "\n${YELLOW}Usage: make <target>${NC}\n\n"} \
		/^[a-zA-Z_0-9-]+:.*?#/ { printf "  ${CYAN}%-15s${NC} %s\n", $$1, $$2 } \
		/^#@/ { printf "\n${BOLD}%s${NC}\n", substr($$0, 4) }' ${MAKEFILE} && echo

all: clean format test build

build: clean # Build hoppr-cyclonedx-models distribution
	${POETRY} lock
	${POETRY} install --sync
	${POETRY} build

clean: # Clean the working directory
	${RM} -r dist .coverage*
	find ${PWD} -name "*.log" -exec ${RM} {} \;

format: black-fix # Format all Python files

black-fix: # Fix formatting with black
	${BLACK} ${TARGETS}

#@ Tests
test: black pylint mypy pytest # Run format check, linting, type checks, and unit tests for all Python files

black: # Check formatting with black
	${BLACK} --check ${TARGETS}

pylint: # Lint code with pylint
	${PYLINT} ${TARGETS}

mypy: # Check static typing with mypy
	${MYPY}

pytest: # Run all unit tests
	${PYTEST}
