## [0.5.5](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.5.4...v0.5.5) (2023-09-27)


### Bug Fixes

* **deps:** update dependency pydantic to v1.10.13 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([06bd561](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/06bd561dff7c43439d892ddd4b65f466af27aa36))

## [0.5.4](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.5.3...v0.5.4) (2023-09-18)


### Bug Fixes

* custom root types as `typing.Annotated` aliases ([eba8316](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/eba83169b06c2296480d84ce86e0f006f77b3fc8))
* field default value direct assignment ([b60df2f](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/b60df2f4171d23c74118372dcc0bc68f8bf64839))
* missing directory ([d8123dd](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/d8123dd3499b2c3d6255f4a95c81a83088f5fd84))
* python 3.7 typing ([017a753](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/017a7534bd1b3c7f3bfa1570cab0694f80aa25ef))


### Reverts

* snake case aliases ([f608122](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/f608122dd6784e0be28ecf76f7e0c818a27c949f))

## [0.5.3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.5.2...v0.5.3) (2023-08-10)


### Bug Fixes

* revert `use-subclass-enum` configuration ([05e0c54](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/05e0c540e9385e2b6ce2325e815e8b796002e295))

## [0.5.2](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.5.1...v0.5.2) (2023-08-08)

## [0.5.1](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.5.0...v0.5.1) (2023-07-27)

## [0.5.0](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.25...v0.5.0) (2023-06-26)


### Features

* Add CycloneDX 1.5 Models ([702c647](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/702c64787494ca30a25e0d9f29fa5ffcbd2d88b1))

## [0.4.25](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.24...v0.4.25) (2023-06-19)


### Bug Fixes

* **deps:** update dependency pydantic to v1.10.9 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f50b19c](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/f50b19cec7ee6024bbbfab1c69a90ea7a812c1b7))

## [0.4.24](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.23...v0.4.24) (2023-05-16)


### Bug Fixes

* remove unused files that are flagged by sast scannners ([066c166](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/066c166bd9365965c886ae7b2bf9673dd0793b46))

## [0.4.23](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.22...v0.4.23) (2023-04-28)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1267fae](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/1267fae8892ed1fabe1e45d675b19bfbe0e1fbf6))

## [0.4.22](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.21...v0.4.22) (2023-04-07)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([85d9e0b](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/85d9e0bedaa3345799d548605107e8c89931d884))

## [0.4.21](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.20...v0.4.21) (2023-04-07)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8082e3d](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8082e3d83fa68616a72ac2d67625bf13b8325226))

## [0.4.20](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.19...v0.4.20) (2023-04-01)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8a8a1d6](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8a8a1d6544b1c1e0e8bdde0c9578f79b0c3f3f1f))

## [0.4.19](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.18...v0.4.19) (2023-03-25)


### Bug Fixes

* **deps:** update dependency semantic-release to v21 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([beac279](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/beac27906c34fb781ceef4a5b921406f458894b2))

## [0.4.18](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.17...v0.4.18) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1f18b6d](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/1f18b6d94fbff337a3b5869cc592d0b270c95a5e))

## [0.4.17](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.16...v0.4.17) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([827f6d9](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/827f6d91c4b45cf45d94d7a3ce5d77b7d71b26bb))

## [0.4.16](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.15...v0.4.16) (2023-03-22)


### Bug Fixes

* **deps:** update dependency pydantic to v1.10.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e29bf86](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e29bf86653699af126b70fdd0d7f71882c8be2e9))

## [0.4.15](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.14...v0.4.15) (2023-03-22)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([219fe12](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/219fe12c1278cde436dcf0f76d9059dab2054282))

## [0.4.14](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.13...v0.4.14) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([730bd7b](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/730bd7b6b7620be86a25ba67d48ebcceed96ff94))

## [0.4.13](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.12...v0.4.13) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2511094](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/25110947d470c1822a1e1e90a22983de767651ea))

## [0.4.12](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.11...v0.4.12) (2023-03-13)


### Bug Fixes

* **deps:** update dependency pydantic to v1.10.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5c6752a](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/5c6752a3e5d0be6e4d9329a316732756d100aff2))

## [0.4.11](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.10...v0.4.11) (2023-03-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f906aba](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/f906aba55bad78e0af8c5369dbb3835ade14e52c))

## [0.4.10](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.9...v0.4.10) (2023-03-02)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([91873e1](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/91873e18d3d7d56c8f30f1663edc50c6bb945dc1))

## [0.4.9](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.8...v0.4.9) (2023-02-28)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6ab3f00](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/6ab3f00ef7b9d813f17e3eb984202bd6eb8b00ac))

## [0.4.8](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.7...v0.4.8) (2023-02-16)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v11 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([243614c](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/243614c385e334c97bae8a28da210b5210eec1a1))

## [0.4.7](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.6...v0.4.7) (2023-02-03)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.1.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1500d46](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/1500d468c6141d60906dfe14bc2dffad38a2b9cd))

## [0.4.6](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.5...v0.4.6) (2023-02-02)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([edd58be](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/edd58be14f1733f706d9bd768b37a17d5cb27a7f))

## [0.4.5](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.4...v0.4.5) (2023-01-31)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.1.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([201aee5](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/201aee5e6e110474b1dd4977c4511d043a4293bd))

## [0.4.4](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.3...v0.4.4) (2023-01-30)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.1.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b72b5a1](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/b72b5a15031a61a2e5ed77b520bc53fdb07a8e9e))

## [0.4.3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.2...v0.4.3) (2023-01-27)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([83291d4](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/83291d4f280f229c354ef782361b53d6538d9057))

## [0.4.2](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.1...v0.4.2) (2023-01-27)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([41c02ba](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/41c02ba6ad80b5bc3279a149ddf0d6a36d618df5))

## [0.4.1](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.4.0...v0.4.1) (2023-01-27)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e722f04](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e722f0456f300f0fff6cfbc2645fbbef9f0bd9f4))

## [0.4.0](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.3.4...v0.4.0) (2023-01-25)


### Features

* add support for Dependency Graph in Model and output serialisation ([783a14c](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/783a14c32f78736a32beebc4c577b7ace49cbb84))
* bump JSON schemas to latest fix verison for 1.2 and 1.3 - see: ([d7e7330](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/d7e73304e1588aa99b5b48878ca402f693b16314))
* Complete support for `bom.components` ([#155](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/155)) ([c0bc23e](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/c0bc23ebb0f3828cfc2da9c1a8149a3e548407e1)), closes [#153](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/153)
* completed work on [#155](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/155) ([#172](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/172)) ([c68bf1d](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/c68bf1dd1ae1983a0b297314cd8f25560f2004fd)), closes [#169](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/169) [#147](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/147)
* support complete model for `bom.metadata` ([#162](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/162)) ([8278b38](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8278b381874afe2c65b948738b7d3bd26a13f380))
* support for `bom.externalReferences` in JSON and XML [#124](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/124) ([bb0a6b7](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/bb0a6b747121c449ac2b4a654bc076b58b9fbf9f))
* support for CycloneDX schema `1.4.2` - adds `vulnerability.properties` to the schema ([9faae3d](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/9faae3db0e75acff812e30599d47ad3c0051c32b))
* support for CycloneDX schema version `1.4.2` ([e2f1514](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e2f1514c9e82d6306d170f05fdd48fbd823a81fe))
* support services in XML BOMs ([d883dad](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/d883dad01462a7f06920224f4f3062154370f448))


### Bug Fixes

* `license_url` not serialised in XML output [#179](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/179) ([#180](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/180)) ([8cc9204](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8cc920426fcf7a3d409285b47acaacb8f429a89f))
* credit cyclonedx-python-lib for test fixtures ([64f24b9](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/64f24b95da59f8009bf18337ec9ad60b4d46d7af))
* further fix for [#150](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/150) ([ff5eaf7](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/ff5eaf7e34700ed30e11722812ca7347fdc351f7))
* properly support nested `components` and `services` [#275](https://gitlab.com/hoppr/hoppr-cyclonedx-models/issues/275) ([6490112](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/6490112efc5a3b7a8a635424e05016e5295d108f))

## [0.3.4](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.3.3...v0.3.4) (2023-01-25)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f3bb943](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/f3bb943f7c065ebfb47eae2c58595b7dfcc36064))

## [0.3.3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.3.2...v0.3.3) (2023-01-24)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.0.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a6af3e3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/a6af3e3f59e47a6ae84d68e643bc6d074467a69a))

## [0.3.2](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.3.1...v0.3.2) (2023-01-23)


### Bug Fixes

* manual renovate coverage to 7.0.0; fix bugs ([00dc6b2](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/00dc6b231237e66bf5fa75017f9c42e100d4a312))

## [0.3.1](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.3.0...v0.3.1) (2023-01-23)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.0.3 ([96ea295](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/96ea2957037bca0edab24ef2571242a1b6905e6a))

## [0.3.0](https://gitlab.com/hoppr/hoppr-cyclonedx-models/compare/v0.2.10...v0.3.0) (2023-01-18)


### Features

* Min python to 3.7.2; Add renovate labels ([9164f75](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/9164f75e994bf1255f1f4fc7456106b8f25ae502))


### Bug Fixes

* Add missing nghttp2-libs pkg ([8b8c9d6](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8b8c9d66d22f76c8db256b726dad1c58fe5b1a69))
* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.0 ([b0e823f](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/b0e823f071e80d64d48b51c0c4eaedf7ce71020e))
* **deps:** update dependency @semantic-release/npm to v9.0.2 ([e38b3fb](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e38b3fbed528ef2bd14c9ae95c5f4cce1c3e6860))
* **deps:** update dependency semantic-release to v20 ([e7fc9d0](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e7fc9d0c418e8625bb8414a1e6bddf94bcbd2d5a))
* **deps:** update dependency semantic-release to v20.0.1 ([e260a6b](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/e260a6bce81191750b98d276ab24c186b4bb4b25))
* **deps:** update dependency semantic-release to v20.0.2 ([617d473](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/617d473d7606a39fe120770faa46426d58f25538))
* **deps:** update semantic-release monorepo ([83fd812](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/83fd812fe4411be98b133eb612c1e86b5266e6ed))
* escape quotes; final regex attempt ([9e5249e](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/9e5249e8f9236350912c206413808f1209037874))
* first version in pyproject.toml ([7065d69](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/7065d6988d083d40f580fce743f6b6c8d718b647))
* job name ([7145727](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/7145727504009c2947ddeaa597d20abd67cf1c4e))
* LM OS author ([0e7356e](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/0e7356e1f73e991d0aac43925d47e7cceca9585b))
* migrate hoppr ci to cdx models ([78926ce](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/78926ce5ecd6fec6f3d67fd03a3be2d17c75896e))
* one last try at semrel version regex ([b718563](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/b7185632e20e5c1e714652a736819d769baa9e67))
* py version; remove job ref ([dd16dec](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/dd16dec7a754c344806c17f83b03332a649955de))
* py versions ([49d1b87](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/49d1b8756779735ec50c963f608a9abef57f89e3))
* remove from pkg.json ([630d1b3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/630d1b37dd5ab9c843280d349f7a1fd814c168f9))
* remove semantic-base ([332d7a3](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/332d7a317c28b01ca5235645abccc9ba1cb62c4e))
* remove semrel-release-replace plugin ([8a5d139](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/8a5d1391cef4c424d8994f3ea326551feeb0b664))
* spaces ([2802803](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/28028035f137d01d80901e00644761951990b7db))
* YAML alignment ([9b28b94](https://gitlab.com/hoppr/hoppr-cyclonedx-models/commit/9b28b94603f70e72a619339db77b099a38fcf6ec))

## [0.2.10](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.9...v0.2.10) (2022-11-03)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.5.0 ([2078a5d](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/2078a5d1085fe90f56c5eaed81d1f3521677784d))

## [0.2.9](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.8...v0.2.9) (2022-09-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.4.2 ([d9045a0](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/d9045a04d973eec15aaf7f357a872bffda20a346))

## [0.2.8](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.7...v0.2.8) (2022-09-06)


### Bug Fixes

* Correct models readme ([66477f4](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/66477f4b2dca738b43493ddf4e33f9a301c418c9))

## [0.2.7](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.6...v0.2.7) (2022-08-29)


### Bug Fixes

* Fix dev vs prod dependencies ([ff66eb1](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/ff66eb14f847abc5c7b41052d881523eba0c5589))

## [0.2.6](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.5...v0.2.6) (2022-08-29)


### Bug Fixes

* Lock git datamodel version ([313f2f8](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/313f2f810e65f1d54ad4daa1d3660a5361747654))
* Lock git datamodel version ([7af0bc8](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/7af0bc8d3f31a09e983c26518e83f2e4217c3b59))

## [0.2.5](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.4...v0.2.5) (2022-08-23)


### Bug Fixes

* **deps:** update dependency semantic-release to v19.0.5 ([64dfcee](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/64dfceef11e5902d7d955f06cfe569f5eafef1cd))

## [0.2.4](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.3...v0.2.4) (2022-08-22)


### Bug Fixes

* **deps:** update dependency semantic-release to v19.0.4 ([c400b20](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/c400b2022b6843dde1c01fa9908f6471fe9f8da0))

## [0.2.3](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.2...v0.2.3) (2022-08-22)


### Bug Fixes

* **deps:** update dependency semantic-release-helm to v2.2.0 ([0fadcef](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/0fadcefe64087814adc50515dca6143c621233f9))

## [0.2.2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.1...v0.2.2) (2022-08-10)


### Bug Fixes

* Allow extra values in model ([f32aa18](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/f32aa183fb7d34d5253f2f65743c8f021b9a5011))

## [0.2.1](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.2.0...v0.2.1) (2022-08-02)


### Bug Fixes

* adjust py.typed file location ([f76ba64](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/f76ba64ddef1528ac8eae59bb20c703e5e3ecaad))
* adjust py.typed file location ([640032b](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/640032bd228d4192e475aa107f633e360e64487b))
* adjust py.typed file location ([afec870](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/afec870c0b1723ced438351307c7b4cdd0b8e5de))

## [0.2.0](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.1.4...v0.2.0) (2022-08-02)


### Features

* add py.typed file ([6387a89](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/6387a89cabd1e6a08f21cf038edeef1db01297d4))
* add py.typed file ([9f85de8](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/9f85de8a27d88c788bf67b7ae5c2c5e7c5c26ae4))

## [0.1.4](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.1.3...v0.1.4) (2022-08-02)


### Bug Fixes

* removing committed binaries. Not going to rewrite history. ([8d563a3](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/8d563a3d4c8c48b4f856b40789db53b7b2d75f08))

## [0.1.3](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.1.2...v0.1.3) (2022-08-02)


### Bug Fixes

* Add LICENSE ([0a18a9d](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/0a18a9d283ba1490cd2acd20f0110d5c4645bdef))

## [0.1.2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.1.1...v0.1.2) (2022-08-01)


### Bug Fixes

* change model-gen to a test, removed pylint test ([9df19c5](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/9df19c5c1bd684e8d52127209b5a7956aad56b82))
* change pylint test and sed replacement in gitlab-ci ([4e7530c](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/4e7530ca5f51036ffb33982299b8c85e8cb40d3b))
* change pylint test and sed replacement in gitlab-ci ([606ad00](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/606ad00486dd990ca24369621a21b3159161b61d))
* changed the sed function to correct error in models ([40c377b](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/40c377bb0eed3f3155217b29770897330b02ab31))
* commenting error with sed that led to parantheses being removed ([d90ced8](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/d90ced8396a7c6447eb7699caa743e95491aecae))
* commenting error with sed that led to parantheses being removed ([9df2893](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/9df2893da98cac53d68543a4c7410376151aa211))

## [0.1.1](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/compare/v0.1.0...v0.1.1) (2022-07-27)


### Bug Fixes

* add generated code to repo ([a00c902](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/a00c9029ed071d1c3442df9307c8451764b314bb))
* add generated code to repo ([54a90fb](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/54a90fb3e0a4e703d116c48c9fbc48f69903fed6))
* Add package.json ([3056a80](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/3056a80052ddff08a6aed45be1c03db1bcffe595))
* changed double quotes ([cb90f94](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/cb90f94a719c3b0b7a283df672392848ac686315))
* check-lint ([7d0c238](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/7d0c2387b2ace1170cf958a6a967925291d72cfe))
* check-lint ([61736db](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/61736db39312b41e092b7d5225172f22eb5773b9))
* check-lint ([ebf8f64](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/ebf8f6442664d398d246a31f0df78883ffba5ae5))
* Correct the ci file name ([a7b3032](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/a7b3032b0d39cb926e47d8a2513212a6b51295b7))
* created poetry files ([58951e2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/58951e255951258b8bd2a414c704b9aa500cda4d))
* edit semantic release file ([2e59dba](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/2e59dba6ea8741c31f8da742d433a68aac0d31d6))
* fix trailing spaces ([c6e28e0](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/c6e28e08fc664d60f4a9f006c13d285376e74227))
* fix trailing spaces ([3d128bf](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/3d128bfb43f0d107f2fd2bad3dada86d39ade0f2))
* gitlab-ci edits and testing ([1551dc3](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/1551dc328549d0753ac02b7d6ef1b020057a2faf))
* gitlab-ci edits and testing ([fd85a1e](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/fd85a1e88585f96cb536d4582e3f95f149b7fce6))
* gitlab-ci edits and testing ([3a90180](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/3a901808ed73d89409353bcdb7fec10fa29ff0bf))
* gitlab-ci file name ([1cb658d](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/1cb658d43924d5a679bb6d151b1f24922bc19ff6))
* gitlab-ci stages ([002a5df](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/002a5dfba3f0544efe8e1548c41dca9aec8c7293))
* Model generation ([ae23d73](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/ae23d735fe08ffb53858abd695b029f21f5e3d81))
* Model generation ([f89a108](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/f89a108c17ec7afc859ffa27ff4dbe79212500d8))
* Model generation ([2564876](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/25648761ac3f6e16988491877ead016e4a3108f4))
* pyrproject file ([6888a00](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/6888a00b7edbecaaa077784387e1d609df57954f))
* pytest update ([3b4d5f2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/3b4d5f295013f1d51f85dbacc8fce5bc50e03efe))
* Pytest Update ([683af59](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/683af591f8ce069a7880209b8b1d94606103ea7c))
* sem file ([2728df2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/2728df2c43335ad6004de3975614fc8e75797a6e))
* sem file ([d2f5ab4](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/d2f5ab463730ea11cb5831e56c9d03d04dc8fc7c))
* sem file ([30209c5](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/30209c5cc0e67f9f1b8975e530e5bd7012447836))
* sem file ([6b76c36](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/6b76c36729d6dc5de4c1d47f3ea3e6731a2971a1))
* semantic-release add ([39d6100](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/39d61003d5f4c4039921ca77f076fcbd672db638))
* test generated code add a branch ([60eabce](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/60eabce9e2457f3b139e3e641fd1352445795c50))
* test generated code add upstream for branch ([f7c9557](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/f7c95579012bacf8bdc24bd86ccf6d1dd02d434b))
* test generated code addition ([4dcc00f](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/4dcc00fd3c615573ad59e72c447c36a6b97176ae))
* test generated code addition ([3e711c4](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/3e711c4c4873c5a06c024bb7ec924b7a3f857f9b))
* test generated code addition ([8579b49](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/8579b4996ede2b7f142faa2891457186c3bb3df8))
* test generated code addition ([d054714](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/d054714b99dcbaf42a5c54b19970b532f0732bb4))
* testing issue ([7d351ba](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/7d351ba03761853838b7ef685cf1d6a1d2b04021))
* trailing spaces ([16ad751](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/16ad7518024f2603423aab973c2521228dcbefd2))
* Update ci file to match hoppr config ([c32eeb2](https://gitlab.com/lmco/hoppr/hoppr-cyclonedx-models/commit/c32eeb27e43e7d5173d40d6828d43867b0e180a8))
