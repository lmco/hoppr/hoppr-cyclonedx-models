#!/usr/bin/env bash

set -euo pipefail

codegen_version="$(poetry run datamodel-codegen --version)"
current_year="$(date +%Y)"
timestamp="$(date --utc --iso-8601=seconds)"

readonly codegen_version
readonly current_year
readonly timestamp

readonly output_dir="output_models"
readonly padding="........................................................"

function generate_models {
  local output="$1"
  local schema_file="$2"
  local output_file="$output"
  local -a extra_args=("${@:3}")

  if [[ -d "$output" ]]; then
    output_file="$output/$(basename --suffix .schema.json "$schema_file")"
    output_file="${output_file//bom-/cyclonedx_}"
    output_file="${output_file//./_}.py"
  fi

  # Render custom file header
  poetry run jinja2 \
    -D "current_year=$current_year" \
    -D "output_file=hoppr_cyclonedx_models/$(basename "$output_file")" \
    -D "schema_file=$(basename "$schema_file")" \
    -D "timestamp=$timestamp" \
    -D "codegen_version=$codegen_version" \
    --outfile datamodel-header.txt \
    templates/datamodel-header.txt.jinja2

  # Generate models
  msg="Generating models from '$(basename "$schema_file")'"
  printf "%s%s" "$msg" "${padding:${#msg}}"
  poetry run datamodel-codegen --input "$schema_file" --output "$output" "${extra_args[@]}"
  echo "DONE"

  if [[ -d "$output" ]]; then
    mv "$output/__init__.py" "$output_file"
  fi

  # Remove unique_items constraints from generated model fields
  sed --regexp-extended --in-place 's/(,\n\s+)?unique_items=True,?//g' "$output_file"
  sed --in-place 's/spdx\.Schema/spdx\.LicenseID/g' "$output_file"
}

# Install jq to parse JSON response
if ! command -v jq &> /dev/null; then
  (apt-get update && apt-get install --yes jq) &> /dev/null
fi

# Get latest tag from GitHub releases API
releases_api="https://api.github.com/repos/CycloneDX/specification/releases/latest"
tag_name="$(curl --fail --silent --show-error --location --url "$releases_api" | jq --raw-output '.tag_name')"

# Download and extract release tarball
release_tar="https://github.com/CycloneDX/specification/archive/refs/tags/$tag_name.tar.gz"
curl --fail --silent --show-error --location --url "$release_tar" | tar xz --no-same-owner

spec_versions=("1.3" "1.4" "1.5" "1.6")
readarray -t schema_files < <(printf "specification-$tag_name/schema/bom-%s.schema.json\n" "${spec_versions[@]}")

mkdir --parents "$output_dir"

for schema_file in "${schema_files[@]}"; do
  # Update "iri-reference" format (unsupported by `datamodel-codegen`) to "uri-reference"
  sed --in-place 's/iri-reference/uri-reference/g' "$schema_file"

  # Update "allOf" definition to "anyOf" to fix model generated for `refLinkType` property
  sed --in-place 's/allOf/anyOf/g' "$schema_file"

  # Remove "licenseChoice" type to fix untyped license list fields
  updated=$(
    jq 'if .definitions.licenseChoice.type == "array" then
        del(.definitions.licenseChoice.type)
      else .
      end' "$schema_file"
  )

  echo "$updated" > "$schema_file"

  generate_models "$output_dir" "$schema_file"
done

generate_models "$output_dir/spdx.py" "specification-$tag_name/schema/spdx.schema.json" --class-name LicenseID
sed --in-place 's/\\_/_/g' "$output_dir"/*.py

msg="Formatting generated model files"
printf "%s%s" "$msg" "${padding:${#msg}}"
poetry run black "$output_dir" &> /dev/null
poetry run isort "$output_dir"
echo "DONE"

mv "$output_dir"/*.py hoppr_cyclonedx_models
